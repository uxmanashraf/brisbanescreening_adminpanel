<?php 

	
	// decoding the json array
	

	$recvData = json_decode(file_get_contents("php://input"), true);
	
	if($recvData != '' ) {

		require_once('includes/db_connect.php'); 

		function pathUrl($dir = __DIR__){

			$root = "";
			$dir = str_replace('\\', '/', realpath($dir));

			//HTTPS or HTTP
			$root .= !empty($_SERVER['HTTPS']) ? 'https' : 'http';

			//HOST
			$root .= '://' . $_SERVER['HTTP_HOST'];

			//ALIAS
			if(!empty($_SERVER['CONTEXT_PREFIX'])) {
				$root .= $_SERVER['CONTEXT_PREFIX'];
				$root .= substr($dir, strlen($_SERVER[ 'CONTEXT_DOCUMENT_ROOT' ]));
			} else {
				$root .= substr($dir, strlen($_SERVER[ 'DOCUMENT_ROOT' ]));
			}

			$root .= '/';

			return $root;
		} 


		$pdf_decoded = base64_decode($recvData['pdf_base64']);
		// here you can detect if type is png or jpg if you want
		$pdf_filename = "pdf_files/pdf" . date('YmdHis') . ".pdf";
		$pdf = fopen ($pdf_filename,'w');

		$msg = '';
		$status = "failed";
		if (fwrite ($pdf,$pdf_decoded)) {
			$status = "success";
			$pdf_path = pathUrl() . $pdf_filename;


			$_query = "INSERT INTO rec_list(id, 
											client_name, 
											job_name, 
											quantity, 
											ameliorants, 
											checkbox1, 
											checkbox2, 
											checkbox3, 
											checkbox4, 
											checkbox5, 
											checkbox6, 
											registration, 
											measurements, 
											total_volume, 
											name, 
											postion, 
											date_pdf, 
											pdf_path
											) 
									VALUES (
											NULL, 
											'". $recvData['client_name'] ."', 
											'". $recvData['job_name'] ."', 
											'". $recvData['quantity'] ."', 
											'". $recvData['ameliorants'] ."', 
											'". $recvData['checkbox1'] ."', 
											'". $recvData['checkbox2'] ."', 
											'". $recvData['checkbox3'] ."', 
											'". $recvData['checkbox4'] ."', 
											'". $recvData['checkbox5'] ."', 
											'". $recvData['checkbox6'] ."', 
											'". $recvData['registration'] ."', 
											'". $recvData['measurements'] ."', 
											'". $recvData['total_volume'] ."', 
											'". $recvData['name'] ."', 
											'". $recvData['postion'] ."', 
											'". $recvData['date_pdf'] ."', 
											'". $pdf_path ."'
			)";
			if (mysqli_query($conn, $_query)) {
				$msg = "data successfully stored";
			} else {
			    $msg = "Error: " . $_query . "<br>" . mysqli_error($conn);
			}


		}

		//close output file
		fclose ($pdf);
		
		$data = array('status' => $status, 'message' => $msg);
		
		echo json_encode($data);

		mysqli_close($conn);

	}

	
	
?>
