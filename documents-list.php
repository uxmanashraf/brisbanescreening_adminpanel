<?php
  session_start();
  if($_SESSION['sid']==session_id())
  {
    $username = $_SESSION['username'];
  }
  else
  {
    header("location:sign-in.php");
  }

?>
<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>Brisbane Screening</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>

    <link rel="icon" href="images/favicon.ico" type="image/x-icon">

    

    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/premium.css">

</head>
<body class=" theme-blue">

    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
            color: #fff;
        }
    </style>

    <script type="text/javascript">
        

        $(document).ready(getAllCat());

        function getAllCat() {
            var posturl = 'includes/getContents.php',
            data =  {'limit': 'false'};
            $.post(posturl, data, function (response) {
                // Response div goes here.
                if(response!=0)
                  $("#docListBody").html(response);
                else {
                }
            });
        }

    </script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    
  

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
   
  <!--<![endif]-->

    <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
        <a class="" href="index.php"><span class="navbar-brand"> <span style="color: #f26522; font-weight: bold;">BRISBANE</span>  <span style="color: #000; font-weight: bold;">SCREENING</span></span></a>
          <!-- <img id="logo" src="images/bs_logo_only.png" style="height: 50px; width: auto; padding: 10px 20px;"> -->
        </div>

        <div class="navbar-collapse collapse" style="height: 1px;">
          <ul id="main-menu" class="nav navbar-nav navbar-right">
            <li class="dropdown hidden-xs">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-user padding-right-small" style="position:relative;top: 3px;"></span> <?php if (isset($username) AND $username!='') {echo $username;} ?>
                    <i class="fa fa-caret-down"></i>
                </a>

              <ul class="dropdown-menu">
                <!-- <li><a href="./">My Account</a></li>
                <li class="divider"></li> -->
                <li><a tabindex="-1" href="logout.php">Logout</a></li>
              </ul>
            </li>
          </ul>

        </div>
      </div>
    </div>
    

    <div class="sidebar-nav">
      <ul>
        <li><a href="#" data-target=".dashboard-menu" class="nav-header" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i> Dashboard<i class="fa fa-collapse"></i></a></li>
        <li><ul class="dashboard-menu nav nav-list collapse in">
                <li><a href="index.php"><span class="fa fa-caret-right"></span> Main</a></li>
                <li class="active"><a href="documents-list.php"><span class="fa fa-caret-right"></span> Records List </a></li>
                <!-- 
                <li ><a href="user.html"><span class="fa fa-caret-right"></span> User Profile</a></li>
                -->
        </ul></li>    
      </ul>
    </div>

    <div class="content">
        <div class="header">
            
            <h1 class="page-title">Records</h1>
                    <ul class="breadcrumb">
            <li><a href="index.php">Home</a> </li>
            <li class="active">Records</li>
        </ul>

        </div>
        <div class="main-content">
            

          <table class="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Client Name</th>
                <th>Job Name</th>
                <th>PDF</th>
                <!-- <th style="width: 3.5em;"></th> -->
              </tr>
            </thead>
            <tbody id="docListBody">
              
            </tbody>
          </table>

          <div class="modal small fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                      <h3 id="myModalLabel">Delete Confirmation</h3>
                  </div>
                  <div class="modal-body">
                      <p class="error-text"><i class="fa fa-warning modal-icon"></i>Are you sure you want to delete the user?<br>This cannot be undone.</p>
                  </div>
                  <div class="modal-footer">
                      <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                      <button class="btn btn-danger" data-dismiss="modal">Delete</button>
                  </div>
                </div>
              </div>
          </div>


          <footer>
              <hr>
                <p class="pull-right"><a href="http://www.xetree.com" target="_blank">Developed by Xetree Solutions</a></p>
                <p>© <a href="http://www.portnine.com" target="_blank">Brisbane Screening </a></p>
          </footer>
        </div>
    </div>


    <script src="lib/bootstrap/js/bootstrap.js"></script>
    
    
  
</body></bs_logo>
